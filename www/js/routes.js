angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  .state('ouEstMaSalle', {
    url: '/',
    templateUrl: 'templates/ouEstMaSalle.html',
    controller: 'ouEstMaSalleCtrl',
	controllerAs: 'asHome'
  })
  .state('cheminLePlusCourt', {
    url: '/cheminLePlusCourt?from&to',
    templateUrl: 'templates/cheminLePlusCourt.html',
    controller: 'cheminLePlusCourtCtrl',
    controllerAs : 'asPath'
  });

  /*
	*/
	$urlRouterProvider.otherwise('/');



});
