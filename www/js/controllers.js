angular.module('app.controllers', [])

.controller('ouEstMaSalleCtrl', ['$scope', '$stateParams', 'graphService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, graphService) {
  var vm = this;

  vm.nodes = graphService.visibleNodes();
  
  vm.scan = function(){
	if (angular.isUndefined(window.cordova)) {
	  alert("Cordova n'est pas chargé");
	  return;
	}
    cordova.plugins.barcodeScanner.scan(
      function (result){
        // alert("we got barcode\n result:" + result.text);
		alert(result.text);
		vm.from = result.text;
        $scope.$apply();
      },
      function (error){
        alert("failed \n result:" + error);
      }
    )
  }

}])

.controller('cheminLePlusCourtCtrl', ['$scope', '$stateParams', 'graphService',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, graphService) {
	var vm=this;
	
	var showPath = function(from, to) {
		return graphService.path(from, to);
	}

	vm.output = showPath($stateParams.from, $stateParams.to);	
}])
