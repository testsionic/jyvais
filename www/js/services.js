angular.module('app.services', [])

.factory('graphService', [function(){
	var graph = new Graph();
	
	graph.nodes = {
			"C201A": new MapNode(2, undefined, undefined, undefined, new Link("Couloir 6")),
			"C202": new MapNode(2, undefined, undefined, new Link("Couloir 5"), undefined),
			"C211": new MapNode(2, undefined, new Link("Couloir 6"), undefined, undefined),
			"C212": new MapNode(2, undefined, undefined, new Link("Couloir 4"), undefined),
			"C223": new MapNode(2, undefined, undefined, undefined, new Link("Couloir 1")),
			"C224": new MapNode(2, undefined, undefined, new Link("Couloir 2"), undefined),
			"C253": new MapNode(2, undefined, undefined, new Link("Couloir 3"), undefined),
			"C254": new MapNode(2, undefined, new Link("Couloir 1"), undefined, undefined),
			"F201": new MapNode(2, new Link("Couloir 14"), undefined, undefined, undefined),
			"F203": new MapNode(2, new Link("Couloir 13"), undefined, undefined, undefined),
			"F205": new MapNode(2, new Link("Couloir 11"), undefined, undefined, undefined),
			"F207": new MapNode(2, new Link("Couloir 10"), undefined, undefined, undefined),
			"F211": new MapNode(2, new Link("Couloir 9"), undefined, undefined, undefined),
			"F213": new MapNode(2, new Link("Couloir 8"), undefined, undefined, undefined),
			"F215": new MapNode(2, new Link("Couloir 12"), undefined, undefined, undefined),
			"G201": new MapNode(2, new Link("Couloir 24"), undefined, undefined, undefined),
			"G201A": new MapNode(2, undefined, undefined, new Link("Couloir 19"), undefined),
			"G206": new MapNode(2, undefined, new Link("Couloir 21"), undefined, undefined),
			"G211": new MapNode(2, new Link("Couloir 19"), undefined, undefined, undefined),
			"G213": new MapNode(2, new Link("Couloir 18"), undefined, undefined, undefined),
			"G214": new MapNode(2, undefined, new Link("Couloir 20"), undefined, undefined),
			"G215": new MapNode(2, new Link("Couloir 17"), undefined, undefined, undefined),
			"G217": new MapNode(2, new Link("Couloir 16"), undefined, undefined, undefined),
			"G218": new MapNode(2, undefined, undefined, undefined, new Link("Couloir 20")),
			"G258": new MapNode(2, new Link("Couloir 23"), undefined, undefined, undefined),
			"G260": new MapNode(2, undefined, new Link("Couloir 16"), undefined, undefined),
			"Ascenseur 1": new MapNode(2, undefined, new Link("Couloir 24"), undefined, undefined, true),
			"Ascenseur 2": new MapNode(2, undefined, new Link("Couloir 15"), undefined, undefined, true),
			"Toilette 1": new MapNode(2, new Link("Couloir 7"), undefined, undefined, undefined),
			"Couloir 1": new MapNode(2, new Link("C254"), new Link("Couloir 2"), new Link("C223"), new Link("C224")),
			"Couloir 2": new MapNode(2, new Link("Couloir 1"), new Link("Couloir 3"), undefined, new Link("C224")),
			"Couloir 3": new MapNode(2, new Link("Couloir 2"), new Link("Couloir 4"), undefined, new Link("C253")),
			"Couloir 4": new MapNode(2, new Link("Couloir 3"), new Link("Couloir 5"), undefined, new Link("C212")),
			"Couloir 5": new MapNode(2, new Link("Couloir 4"), new Link("Couloir 7"), new Link("Couloir 6"), new Link("C202")),
			"Couloir 6": new MapNode(2, new Link("C211"), undefined, new Link("C201A"), new Link("Couloir 5")),
			"Couloir 7": new MapNode(2, new Link("Couloir 5"), new Link("Toilette 1"), new Link("Couloir 8"), new Link("Couloir 11")),
			"Couloir 8": new MapNode(2, new Link("Couloir 7"), new Link("F213"), new Link("Couloir 12"), new Link("Couloir 9")),
			"Couloir 9": new MapNode(2, undefined, new Link("F211"), new Link("Couloir 8"), new Link("Couloir 10")),
			"Couloir 10": new MapNode(2, undefined, new Link("F207"), new Link("Couloir 9"), new Link("Couloir 11")),
			"Couloir 11": new MapNode(2, new Link("Couloir 7"), new Link("F205"), new Link("Couloir 10"), new Link("Couloir 13")),
			"Couloir 12": new MapNode(2, undefined, new Link("F215"), undefined, new Link("Couloir 8")),
			"Couloir 13": new MapNode(2, undefined, new Link("F203"), new Link("Couloir 11"), new Link("Couloir 14")),
			"Couloir 14": new MapNode(2, undefined, new Link("F201"), new Link("Couloir 13"), new Link("Couloir 15")),
			"Couloir 15": new MapNode(2, new Link("Ascenseur 2"), undefined, new Link("Couloir 14"), new Link("Couloir 16")),
			"Couloir 16": new MapNode(2, new Link("G260"), new Link("G217"), new Link("Couloir 15"), new Link("Couloir 17")),
			"Couloir 17": new MapNode(2, undefined, new Link("G215"), new Link("Couloir 16"), new Link("Couloir 18")),
			"Couloir 18": new MapNode(2, new Link("Couloir 20"), undefined, new Link("Couloir 17"), new Link("Couloir 19")),
			"Couloir 19": new MapNode(2, new Link("Couloir 21"), undefined, new Link("Couloir 18"), new Link("G201A")),
			"Couloir 20": new MapNode(2, new Link("G214"), new Link("Couloir 18"), new Link("G218"), new Link("Couloir 21")),
			"Couloir 21": new MapNode(2, new Link("G206"), new Link("Couloir 19"), new Link("Couloir 20"), new Link("Couloir 22")),
			"Couloir 22": new MapNode(2, undefined, new Link("G258"), new Link("Couloir 21"), new Link("Couloir 23")),
			"Couloir 23": new MapNode(2, undefined, new Link("G258"), new Link("Couloir 22"), new Link("Couloir 24")),
			"Couloir 24": new MapNode(2, new Link("Ascenseur 1"), new Link("G201"), new Link("Couloir 23"), undefined),
		};
		
	var getGraph = function() {
		return graph;
	};
	
	var getPath = function(from, to)  {
		return graph.path(from, to).directions();
	};
	
	var getVisibleNodes = function() {
		var visibleNodes = [];
		angular.forEach(graph.nodes, function(value, key) {
			var isVisible = key.substring(0, 7) !== 'Couloir';
			if (isVisible) {
				visibleNodes.push(key);
			}
		});
		return visibleNodes;
	};
	
	return {
        "graph": getGraph,
		"path" : getPath,
		"visibleNodes" : getVisibleNodes
    }
}])

.service('BlankService', [function(){

}]);