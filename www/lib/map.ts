const enum Direction {
    North, East, South, West, Elevator, Stairs
}

const FULL_CIRCLE = 360;
let angleOf = (direction: Direction): number => {
    switch (direction) {
    case Direction.East:
        return 0;
    case Direction.South:
        return 90;
    case Direction.West:
        return 180;
    case Direction.North:
        return 270;
    }
    return 0;
};

let reverseDirection = (direction?: Direction): Direction | undefined => {
    if (direction === undefined) {
        return undefined;
    }
    switch (direction) {
    case Direction.East:
        return Direction.West;
    case Direction.South:
        return Direction.North;
    case Direction.West:
        return Direction.East;
    case Direction.North:
        return Direction.South;
    }
    return undefined;
};

const enum Move {
    Forward, TurnLeft, TurnRight, Finish
}

let moveFromDirectionToDirection = (from: Direction, to: Direction): Move | undefined =>  {
    let fromAngle = angleOf(from);
    let toAngle = angleOf(to);

    let difference = ((toAngle + FULL_CIRCLE) - fromAngle) % FULL_CIRCLE;
    switch (difference) {
    case 0:
        return Move.Forward;
    case 90:
        return Move.TurnRight;
    case 270:
        return Move.TurnLeft;
    }
    return undefined;
};

class Link {
    static NO_ORIGIN = '-';

    destination: string;
    distance: number;

    get origin(): string {
        return this.destination;
    }
    set origin(value: string) {
        this.destination = value;
    }

    constructor(destination: string, distance: number = 1) {
        this.destination = destination;
        this.distance = distance;
    }
}

class MapNode {
    north?: Link;
    south?: Link;
    west?: Link;
    east?: Link;

    floor: number;
    isElevator: boolean;
    vertical: Link[] = [];

    get links(): Link[] {
        let links: Link[] = [];
        this.north && links.push(this.north);
        this.south && links.push(this.south);
        this.west && links.push(this.west);
        this.east && links.push(this.east);
        return links;
    }
    
    constructor(floor: number, north?: Link, south?: Link, west?: Link, east?: Link, isElevator: boolean = false) {
        this.floor = floor
        this.north = north
        this.south = south
        this.west = west
        this.east = east
        this.isElevator = isElevator
    }

    linkTo(node: string): Link | undefined {
        for (let link of this.links) {
            if (link.destination === node) {
                return link;
            }
        }
        return undefined;
    }
    linkAt(direction: Direction): Link | undefined {
        switch(direction) {
        case Direction.North:
            return this.north;
        case Direction.South:
            return this.south;
        case Direction.West:
            return this.west;
        case Direction.East:
            return this.east;
        }
        return undefined;
    }
    directionTo(node: string): Direction | undefined {
        for (let direction of [Direction.North, Direction.East, Direction.South, Direction.West]) {
            let link = this.linkAt(direction);
            if (link !== undefined && link.destination === node) {
                return direction;
            }
        }
        return undefined;
    }
    increaseMovesFrom(direction: Direction, moves: {[key: number]: number}) {
        for (let link of this.links) {
            let nextDirection = this.directionTo(link.destination);
            if (nextDirection !== undefined) {
                let move = moveFromDirectionToDirection(direction, nextDirection);
                if (move !== undefined) {
                    moves[move] = moves[move] !== undefined ? moves[move] + 1 : 1;
                }
            }
        }
    }
}

class Path {
    constructor(private from: string, private to: string, private path: Link[], private nodes: {[key: string]: MapNode}) {}

    directions(): {move: Move, text: string}[] {
        let directions: {move: Move, text: string}[] = [];

        let previousNodeName = this.from;
        let previousMove: Move | undefined;

        let moveCount: {[key: number]: number} = {};

        for (let index = 0; index < this.path.length - 1; index++) {
            let link = this.path[index];
            let nextLink = this.path[index + 1];

            let node = this.nodes[link.destination];
            let currentDirection = reverseDirection(node.directionTo(previousNodeName));
            let direction = node.directionTo(nextLink.destination);

            let move: Move | undefined = undefined;
            if (currentDirection === undefined || direction === undefined) {
                continue;
            }
            let result = moveFromDirectionToDirection(currentDirection, direction);
            if (result != null) {
                move = result;
            }
            if (move === undefined) {
                continue;
            }
            node.increaseMovesFrom(currentDirection, moveCount);
            
            let phrase = '';
            switch (move) {
            case Move.Forward:
                phrase = 'Aller tout droit';
                break;
            case Move.TurnLeft:
                if (moveCount[Move.TurnLeft] === undefined || moveCount[Move.TurnLeft] <= 1) {
                    phrase = 'Tourner à gauche';
                } else {
                    phrase = `Prendre la ${moveCount[Move.TurnLeft]}ème à gauche`;
                }
                break;
            case Move.TurnRight:
                if (!moveCount[Move.TurnRight] === undefined || moveCount[Move.TurnRight] <= 1) {
                    phrase = 'Tourner à droite';
                } else {
                    phrase = `Prendre la ${moveCount[Move.TurnRight]}ème à droite`;
                }
                break;
            }
            if (directions.length === 0) {
                phrase = 'En sortant de votre bureau, ' + phrase.toLowerCase();
            }

            if (directions.length === 0 || move !== Move.Forward || directions[directions.length - 1].move !== Move.Forward) {
                directions.push({move: move, text: phrase});
                moveCount = {};
            }
            previousNodeName = link.destination;
            previousMove = move;
        }

        directions.push({move: Move.Finish, text: `Vous êtes arrivé au bureau ${this.to}.`});

        return directions;
    }
}

class Graph {
    nodes: {[key: string]: MapNode};

    path(from: string, to: string): Path {
        let shortestDistances = this.createDistanceDictionary(from);
        let visits: {[key: string]: boolean} = {};

        let currentNode = from;
        let currentDistance = 0;

        while (currentNode !== to) {
            visits[currentNode] = true;
            this.updateDistances(currentNode, currentDistance, shortestDistances);
            const shortest = this.shortestUnvisitedLink(currentNode, shortestDistances, visits);
            currentNode = shortest.destination;
            currentDistance = shortest.distance;
        }

        return this.createPath(from, to, currentDistance, shortestDistances);
    }

    private createDistanceDictionary(start: string): {[key: string]: Link} {
        let dictionary: {[key: string]: Link} = {};
        for (let key in this.nodes) {
            dictionary[key] = new Link(Link.NO_ORIGIN, Number.MAX_SAFE_INTEGER);
        }
        dictionary[start].distance = 0;
        return dictionary;
    }

    private updateDistances(from: string, distance: number, distances: {[key: string]: Link}) {
        const fromNode = this.nodes[from];

        for (let link of fromNode.links) {
            const distanceToLink = distance + link.distance;
            if (distanceToLink < distances[link.destination].distance) {
                distances[link.destination].distance = distanceToLink;
                distances[link.destination].origin = from;
            }
        }
    }

    private shortestUnvisitedLink(from: string, distances: {[key: string]: Link}, visits: {[key: string]: boolean}): Link {
        let minimum = new Link('', Number.MAX_SAFE_INTEGER);
        
        for (let key in distances) {
            const distance = distances[key];
            if (!visits[key] && distance.distance <= minimum.distance) {
                minimum.distance = distance.distance;
                minimum.destination = key;
            }
        }

        return minimum;
    }

    private createPath(from: string, to: string, distance: number, shortestDistances: {[key: string]: Link}): Path {
        let path: Link[] = [];
        let currentNode = to;
        let currentDistance = distance;

        while (currentNode !== from) {
            let origin = shortestDistances[currentNode];
            path.unshift(new Link(currentNode, currentDistance));

            let originNode = this.nodes[origin.origin];
            let link = originNode.linkTo(currentNode);
            if (link != null) {
                currentDistance -= link.distance;
            }
            currentNode = origin.origin;
        }
        path.unshift(new Link(from, 0));
        return new Path(from, to, path, this.nodes);
    }
}
