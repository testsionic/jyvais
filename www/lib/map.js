var FULL_CIRCLE = 360;
var angleOf = function (direction) {
    switch (direction) {
        case 1 /* East */:
            return 0;
        case 2 /* South */:
            return 90;
        case 3 /* West */:
            return 180;
        case 0 /* North */:
            return 270;
    }
    return 0;
};
var reverseDirection = function (direction) {
    if (direction === undefined) {
        return undefined;
    }
    switch (direction) {
        case 1 /* East */:
            return 3 /* West */;
        case 2 /* South */:
            return 0 /* North */;
        case 3 /* West */:
            return 1 /* East */;
        case 0 /* North */:
            return 2 /* South */;
    }
    return undefined;
};
var moveFromDirectionToDirection = function (from, to) {
    var fromAngle = angleOf(from);
    var toAngle = angleOf(to);
    var difference = ((toAngle + FULL_CIRCLE) - fromAngle) % FULL_CIRCLE;
    switch (difference) {
        case 0:
            return 0 /* Forward */;
        case 90:
            return 2 /* TurnRight */;
        case 270:
            return 1 /* TurnLeft */;
    }
    return undefined;
};
var Link = (function () {
    function Link(destination, distance) {
        if (distance === void 0) { distance = 1; }
        this.destination = destination;
        this.distance = distance;
    }
    Object.defineProperty(Link.prototype, "origin", {
        get: function () {
            return this.destination;
        },
        set: function (value) {
            this.destination = value;
        },
        enumerable: true,
        configurable: true
    });
    return Link;
}());
Link.NO_ORIGIN = '-';
var MapNode = (function () {
    function MapNode(floor, north, south, west, east, isElevator) {
        if (isElevator === void 0) { isElevator = false; }
        this.vertical = [];
        this.floor = floor;
        this.north = north;
        this.south = south;
        this.west = west;
        this.east = east;
        this.isElevator = isElevator;
    }
    Object.defineProperty(MapNode.prototype, "links", {
        get: function () {
            var links = [];
            this.north && links.push(this.north);
            this.south && links.push(this.south);
            this.west && links.push(this.west);
            this.east && links.push(this.east);
            return links;
        },
        enumerable: true,
        configurable: true
    });
    MapNode.prototype.linkTo = function (node) {
        for (var _i = 0, _a = this.links; _i < _a.length; _i++) {
            var link = _a[_i];
            if (link.destination === node) {
                return link;
            }
        }
        return undefined;
    };
    MapNode.prototype.linkAt = function (direction) {
        switch (direction) {
            case 0 /* North */:
                return this.north;
            case 2 /* South */:
                return this.south;
            case 3 /* West */:
                return this.west;
            case 1 /* East */:
                return this.east;
        }
        return undefined;
    };
    MapNode.prototype.directionTo = function (node) {
        for (var _i = 0, _a = [0 /* North */, 1 /* East */, 2 /* South */, 3 /* West */]; _i < _a.length; _i++) {
            var direction = _a[_i];
            var link = this.linkAt(direction);
            if (link !== undefined && link.destination === node) {
                return direction;
            }
        }
        return undefined;
    };
    MapNode.prototype.increaseMovesFrom = function (direction, moves) {
        for (var _i = 0, _a = this.links; _i < _a.length; _i++) {
            var link = _a[_i];
            var nextDirection = this.directionTo(link.destination);
            if (nextDirection !== undefined) {
                var move = moveFromDirectionToDirection(direction, nextDirection);
                if (move !== undefined) {
                    moves[move] = moves[move] !== undefined ? moves[move] + 1 : 1;
                }
            }
        }
    };
    return MapNode;
}());
var Path = (function () {
    function Path(from, to, path, nodes) {
        this.from = from;
        this.to = to;
        this.path = path;
        this.nodes = nodes;
    }
    Path.prototype.directions = function () {
        var directions = [];
        var previousNodeName = this.from;
        var previousMove;
        var moveCount = {};
        for (var index = 0; index < this.path.length - 1; index++) {
            var link = this.path[index];
            var nextLink = this.path[index + 1];
            var node = this.nodes[link.destination];
            var currentDirection = reverseDirection(node.directionTo(previousNodeName));
            var direction = node.directionTo(nextLink.destination);
            var move = undefined;
            if (currentDirection === undefined || direction === undefined) {
                continue;
            }
            var result = moveFromDirectionToDirection(currentDirection, direction);
            if (result != null) {
                move = result;
            }
            if (move === undefined) {
                continue;
            }
            node.increaseMovesFrom(currentDirection, moveCount);
            var phrase = '';
            switch (move) {
                case 0 /* Forward */:
                    phrase = 'Aller tout droit';
                    break;
                case 1 /* TurnLeft */:
                    if (moveCount[1 /* TurnLeft */] === undefined || moveCount[1 /* TurnLeft */] <= 1) {
                        phrase = 'Tourner à gauche';
                    }
                    else {
                        phrase = "Prendre la " + moveCount[1 /* TurnLeft */] + "\u00E8me \u00E0 gauche";
                    }
                    break;
                case 2 /* TurnRight */:
                    if (!moveCount[2 /* TurnRight */] === undefined || moveCount[2 /* TurnRight */] <= 1) {
                        phrase = 'Tourner à droite';
                    }
                    else {
                        phrase = "Prendre la " + moveCount[2 /* TurnRight */] + "\u00E8me \u00E0 droite";
                    }
                    break;
            }
            if (directions.length === 0) {
                phrase = 'En sortant de votre bureau, ' + phrase.toLowerCase();
            }
            if (directions.length === 0 || move !== 0 /* Forward */ || directions[directions.length - 1].move !== 0 /* Forward */) {
                directions.push({ move: move, text: phrase });
                moveCount = {};
            }
            previousNodeName = link.destination;
            previousMove = move;
        }
        directions.push({ move: 3 /* Finish */, text: "Vous \u00EAtes arriv\u00E9 au bureau " + this.to + "." });
        return directions;
    };
    return Path;
}());
var Graph = (function () {
    function Graph() {
    }
    Graph.prototype.path = function (from, to) {
        var shortestDistances = this.createDistanceDictionary(from);
        var visits = {};
        var currentNode = from;
        var currentDistance = 0;
        while (currentNode !== to) {
            visits[currentNode] = true;
            this.updateDistances(currentNode, currentDistance, shortestDistances);
            var shortest = this.shortestUnvisitedLink(currentNode, shortestDistances, visits);
            currentNode = shortest.destination;
            currentDistance = shortest.distance;
        }
        return this.createPath(from, to, currentDistance, shortestDistances);
    };
    Graph.prototype.createDistanceDictionary = function (start) {
        var dictionary = {};
        for (var key in this.nodes) {
            dictionary[key] = new Link(Link.NO_ORIGIN, Number.MAX_SAFE_INTEGER);
        }
        dictionary[start].distance = 0;
        return dictionary;
    };
    Graph.prototype.updateDistances = function (from, distance, distances) {
        var fromNode = this.nodes[from];
        for (var _i = 0, _a = fromNode.links; _i < _a.length; _i++) {
            var link = _a[_i];
            var distanceToLink = distance + link.distance;
            if (distanceToLink < distances[link.destination].distance) {
                distances[link.destination].distance = distanceToLink;
                distances[link.destination].origin = from;
            }
        }
    };
    Graph.prototype.shortestUnvisitedLink = function (from, distances, visits) {
        var minimum = new Link('', Number.MAX_SAFE_INTEGER);
        for (var key in distances) {
            var distance = distances[key];
            if (!visits[key] && distance.distance <= minimum.distance) {
                minimum.distance = distance.distance;
                minimum.destination = key;
            }
        }
        return minimum;
    };
    Graph.prototype.createPath = function (from, to, distance, shortestDistances) {
        var path = [];
        var currentNode = to;
        var currentDistance = distance;
        while (currentNode !== from) {
            var origin = shortestDistances[currentNode];
            path.unshift(new Link(currentNode, currentDistance));
            var originNode = this.nodes[origin.origin];
            var link = originNode.linkTo(currentNode);
            if (link != null) {
                currentDistance -= link.distance;
            }
            currentNode = origin.origin;
        }
        path.unshift(new Link(from, 0));
        return new Path(from, to, path, this.nodes);
    };
    return Graph;
}());
//# sourceMappingURL=map.js.map